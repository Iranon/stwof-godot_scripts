extends Spatial

var explosion_scene = preload("res://ExplosionScene.tscn")
var camera_rot

var impact_scene = preload("res://ImpactArea.tscn")

var laser_vel
var hit = false
var laser_hit_transform

#FX function
func boom():
	#Add ExplosionScene
	var explosion = explosion_scene.instance()
	var scene_root = get_tree().root.get_children()[1] #getting the root([1] depends on singleton)
	scene_root.get_node("EarthOrigin/Earth").add_child(explosion)
	
	#Set explosion instance
	explosion.global_transform = laser_hit_transform
	camera_rot = get_parent().get_node("Camera")
	#explosion.set_rotation(Vector3(self.rotation.x, self.rotation.y, camera_rot.rotation.z))
	explosion.look_at(camera_rot.get_translation(), Vector3(0, 0, 1))
	
	#Add ImpactArea effect
	var impact = impact_scene.instance()
	scene_root.get_node("EarthOrigin/Earth").add_child(impact)
	impact.global_transform = laser_hit_transform
	impact.set_scale(Vector3(1.2, 1.2, 1.2))

func _ready():
	laser_vel = 10

func _process(delta):
	#Set laser movement.
	var direction = - global_transform.basis.y.normalized()
	global_translate(direction * laser_vel * delta)
	

func _on_Area_area_shape_entered(_area_id, _area, _area_shape, _self_shape):
	laser_hit_transform = get_transform() #Coordinate needed to instance the explosion
	hit = true
	boom()
	#Remove Laser
	hide()
	queue_free()
