extends Spatial


func _ready():
	#Play the animation after instancing
	$Viewport/ExplAnimatedSprite.play()


#Remove the explosion scene at the end of the animation
func _on_ExplAnimatedSprite_animation_finished():
	hide()
	queue_free()
