extends Spatial


func _ready():
	#Start the animation after instancing
	$Viewport/ImpactAnimatedSprite.play()


#Remove the impact ring scene at the end of the animation
func _on_ImpactAnimatedSprite_animation_finished():
	hide()
	queue_free()
