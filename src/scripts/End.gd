extends Spatial

func _ready():
	pass

func _process(_delta):
	if Input.is_action_just_pressed("ui_cancel") :
		get_tree().quit()
	
	if Input.is_key_pressed(KEY_R) :
		#Reset status
		get_node("/root/Global").win = false
		get_node("/root/Global").targets = 4
		# warning-ignore:return_value_discarded
		get_tree().change_scene("res://Space.tscn")
