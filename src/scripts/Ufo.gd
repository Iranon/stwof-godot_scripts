extends MeshInstance


onready var laser_sound = get_node("LaserStream")
var laser_scene = preload("res://LaserScene.tscn")

var fire_rate = 0.6
var armed
func shooting():
	var laser_shot = laser_scene.instance()
	var scene_root = get_tree().root.get_children()[1] #getting the root([1] depends on singleton)
	scene_root.add_child(laser_shot)
	laser_shot.set_rotation(get_parent().rotation)
	laser_shot.global_transform = self.global_transform
	

func _ready():
	armed = true

func _process(delta):
	transform.basis = transform.basis.rotated(Vector3(0, 1, 0), 0.8 * delta)
	if Input.is_action_pressed("ui_home") and (armed) :
		shooting()
		$LaserParticles.set_emitting(true)
		laser_sound.play()
		armed = false
		#Timer
		yield(get_tree().create_timer(fire_rate), "timeout")
		armed = true
