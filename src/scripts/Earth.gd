extends MeshInstance

var rand_gen = RandomNumberGenerator.new() #class to generate random numbers
var rand_numb

var earth_mat
var earth_life

func _ready():
	earth_mat = get_surface_material(0) #get the first material (index 0)
	earth_life = 1.0 #variable to manage Earth textures blending
	
	#Generate a random number
	rand_gen.randomize()
	rand_numb = rand_gen.randf_range(0.1, 0.4)

func _process(delta):
	
	#Start rotating Earth
	transform.basis = transform.basis.rotated(Vector3(1, 0, 0), 0.3 * delta)
	transform.basis = transform.basis.rotated(Vector3(0, 1, 0), rand_numb * delta)
	transform.basis = transform.basis.rotated(Vector3(0, 0, 1), 0.6 * delta)
	#transform = transform.orthonormalized()
	
	#Pass the variable to the shader
	earth_mat.set_shader_param("earth_life", earth_life)


#The value get modified on each collision with the laser
func _on_Area_area_shape_entered(_area_id, area, _area_shape, _self_shape):
	var collider_name = area.get_parent().get_parent().get_name()
	if collider_name == "Laser" :
		if earth_life > 0 :
			earth_life -= 0.06
		else:
			earth_life = 0.0
