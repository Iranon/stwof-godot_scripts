extends Spatial

var rot_vel = deg2rad(80)

func _ready():
	pass

#Rotate the UFO from the pivot-point
func _process(delta):
	if Input.is_action_pressed("ui_left") :
		rotate_object_local(Vector3(0, 0, 1), rot_vel  * delta)
	if Input.is_action_pressed("ui_right") :
		rotate_object_local(Vector3(0, 0, 1), -rot_vel * delta)
	if Input.is_action_pressed("ui_up") :
		rotate_object_local(Vector3(1, 0, 0), -rot_vel * delta)
	if Input.is_action_pressed("ui_down") :
		rotate_object_local(Vector3(1, 0, 0), rot_vel * delta)
