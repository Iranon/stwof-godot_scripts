extends Spatial


func _ready():
	pass


func _process(_delta):
	if get_node("/root/Global").win :
		get_node("End").play("End_anim")
		yield(get_node("End"), "animation_finished") #wait the animation
		#Push final scene
		# warning-ignore:return_value_discarded
		get_tree().change_scene("res://End.tscn")
	
	#Press Escape to quit the game
	if Input.is_action_just_pressed("ui_cancel") :
		get_tree().quit()
	
	#Press 'R' to reload the scene
	if Input.is_key_pressed(KEY_R) :
		get_node("/root/Global").targets = 4 #reset targets status
		# warning-ignore:return_value_discarded
		get_tree().reload_current_scene()
