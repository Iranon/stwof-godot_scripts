extends MeshInstance

var stricken = false #useful to avoid double-hit on the same target

func _ready():
	pass

func _process(_delta):
	pass


func _on_Area_area_shape_entered(_area_id, area, _area_shape, _self_shape):
	var collider_name = area.get_parent().get_parent().get_name()
	if collider_name == "Laser" and not stricken :
		get_node("/root/Global").targets -= 1
		stricken = true
		get_node("AnimationPlayer").play("HitFX_anim")
		yield(get_node("AnimationPlayer"), "animation_finished")
		stricken = false
		hide()
		queue_free()
