extends Control


var start_selected #boolean variable to control the menu selection
var start_scene = preload("res://Space.tscn") #preload next scene


func _ready():
	
	$ColorRect/VBoxContainer/Label/SelectionRec1.show()
	$ColorRect/VBoxContainer/Label2/SelectionRec2.hide()
	start_selected = true


func _process(_delta):
	#Handle menu voices selection
	if Input.is_action_just_pressed("ui_down") :
		$ColorRect/VBoxContainer/Label/SelectionRec1.hide()
		$ColorRect/VBoxContainer/Label2/SelectionRec2.show()
		start_selected = false
	
	if Input.is_action_just_pressed("ui_up") :
		$ColorRect/VBoxContainer/Label/SelectionRec1.show()
		$ColorRect/VBoxContainer/Label2/SelectionRec2.hide()
		start_selected = true
	
	#Setting up start game selection
	if start_selected and Input.is_action_just_pressed("ui_accept") :
		#Play the zoom animation
		get_parent().get_parent().get_node("FadesAnimationPlayer").play("Start_game_Anim")
		#Wait the end of the animation to switch scene
		yield(get_parent().get_parent().get_node("FadesAnimationPlayer"), "animation_finished")
		#Start the level
		# warning-ignore:return_value_discarded
		get_tree().change_scene("res://Space.tscn")
	
	#Setting up exit selection
	if not start_selected and Input.is_action_just_pressed("ui_accept") :
		get_tree().quit() #quit game
