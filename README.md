# STWOF-Godot_scripts

Godot scripts of a little project (originally started for the first *Let's Code Games* game jam).

You can obtain the executable (Linux and Windows) on itch.io **[here](https://iranon.itch.io/set-the-world-on-fire)**.